import { Component, OnInit } from '@angular/core';
import { Enclot } from "../enclot";
import { FormGroup, FormBuilder, Validators } from '@angular/forms';


@Component({
  selector: 'app-liste-enclots',
  templateUrl: './liste-enclots.component.html',
  styleUrls: ['./liste-enclots.component.css']
})
export class ListeEnclotsComponent implements OnInit {

  enclots : Enclot[] = []
  enclotName : string

  form: FormGroup;

  constructor(private formBuilder : FormBuilder) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      enclotName : [ '', [
        Validators.required,
        Validators.minLength(4)
      ]]
    });

    this.displayEnclots();
  }

  private displayEnclots() {
    const enclots = localStorage.getItem('enclots');
    this.enclots = enclots ? JSON.parse(enclots) : [];
  }

  saveEnclots() {
    localStorage.setItem('enclots', JSON.stringify(this.enclots));
  }

  addEnclots() {
    this.enclots.push({
      id : this.enclots.reduce((acc, e) => acc <= e.id ? e.id + 1 : acc, 1),
      name : this.enclotName,
      animaux : [],
    });
    this.saveEnclots();
    this.enclotName = '';
  }

  deleteEnclot(enclot : Enclot) {
    this.enclots = this.enclots.filter(e => e.id !== enclot.id)
    this.saveEnclots()
  }
}
