import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeEnclotsComponent } from './liste-enclots.component';

describe('ListeEnclotsComponent', () => {
  let component: ListeEnclotsComponent;
  let fixture: ComponentFixture<ListeEnclotsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeEnclotsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeEnclotsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
