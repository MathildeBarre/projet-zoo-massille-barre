import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'reverseName'
})
export class ReverseName implements PipeTransform {
  transform(value: string): string {
    let newName: string = "";
    for (let i = value.length - 1; i >= 0; i--) {
      newName += value.charAt(i);
    }
    return newName;
  }
}
