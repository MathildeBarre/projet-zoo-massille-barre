import { Component, OnInit, Input } from '@angular/core';
import { Animal } from '../animal';
import { Enclot } from '../enclot';
import { ListeEnclotsComponent } from '../liste-enclots/liste-enclots.component';

@Component({
  selector: 'app-liste-animaux',
  templateUrl: './liste-animaux.component.html',
  styleUrls: ['./liste-animaux.component.css']
})
export class ListeAnimauxComponent implements OnInit {

  @Input()
  enclot : Enclot;
  
  animals : Animal[] = [];
  animalName : string

  constructor(private listeEnclots :ListeEnclotsComponent) { }

  ngOnInit() {
  }

  saveAnimals() {
    this.enclot.animaux = [];
    this.enclot.animaux.push(this.animals);
    this.listeEnclots.saveEnclots();
  }

  addAnimals(){
    this.animals.push({
      id : this.animals.reduce((acc, a) => acc <= a.id ? a.id + 1 : acc, 1),
      name : this.animalName
    })
    this.saveAnimals();
    this.animalName = '';
  }

  deleteAnimals(){

  }
}
