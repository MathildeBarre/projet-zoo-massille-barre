import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { ListeEnclotsComponent } from './liste-enclots/liste-enclots.component';
import { EnclotsComponent } from './enclots/enclots.component';
import { FormsModule, FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import { ListeAnimauxComponent } from './liste-animaux/liste-animaux.component';
import { AnimalComponent } from './animal/animal.component';
import {MatTreeModule} from '@angular/material/tree';
import {MatDividerModule} from '@angular/material/divider';
import {MatListModule} from '@angular/material/list';
import { ReverseName } from './reverse-name.pipe';

@NgModule({
  declarations: [
    AppComponent,
    ListeEnclotsComponent,
    EnclotsComponent,
    ListeAnimauxComponent,
    AnimalComponent,
    ReverseName,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatButtonModule,
    MatInputModule,
    MatTreeModule,
    MatDividerModule,
    MatListModule,
  ],
  providers: [FormBuilder],
  bootstrap: [AppComponent]
})
export class AppModule { }
