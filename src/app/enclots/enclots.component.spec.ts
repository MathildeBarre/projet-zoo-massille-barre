import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnclotsComponent } from './enclots.component';

describe('EnclotsComponent', () => {
  let component: EnclotsComponent;
  let fixture: ComponentFixture<EnclotsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnclotsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnclotsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
