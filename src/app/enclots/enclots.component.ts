import { Component, Output, EventEmitter, Input, OnInit } from "@angular/core";
import { Enclot } from "../enclot";

@Component({
  selector: 'app-enclots',
  templateUrl: './enclots.component.html',
  styleUrls: ['./enclots.component.css']
})
export class EnclotsComponent implements OnInit {

  @Output()
  delete = new EventEmitter<Enclot>();

  @Input()
  enclot : Enclot;

  deleting = false


  constructor() { }

  ngOnInit() {
  }

  deleteThisOne() {
    this.deleting = true;
    this.delete.emit(this.enclot);
  }
}
